package com.example.parrot.pong1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;

public class Ball {

            private Rect ball;

    private int ballPositionX;
    private int ballPositionY;



    public Ball( int x, int y)
    {
        this.ballPositionX = x;
        this.ballPositionY = y;


    }


    public Rect getBall() {
        return ball;
    }

    public void setBall(Rect ball) {
        this.ball = ball;
    }

    public int getxPosition() {
        return ballPositionX;
    }

    public void setxPosition(int xPosition) {
        this.ballPositionX = xPosition;
    }

    public int getyPosition() {
        return ballPositionY;
    }

    public void setyPosition(int yPosition) {
        this.ballPositionY = yPosition;
    }
}
