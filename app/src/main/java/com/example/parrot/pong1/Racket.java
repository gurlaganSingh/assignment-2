package com.example.parrot.pong1;

import android.content.Context;
import android.graphics.Rect;

public class Racket {
    private Rect racket;

    private int racketPositionX;
    private int racketPositionY;


    public Racket( int x, int y)
    {
        this.racketPositionX = x;
        this.racketPositionY = y;


    }

    public Rect getRacket() {
        return racket;
    }

    public void setRacket(Rect racket) {
        this.racket = racket;
    }

    public int getxRacketPosition() {
        return racketPositionX;
    }

    public void setxRacketPosition(int xRacketPosition) {
        this.racketPositionX = xRacketPosition;
    }

    public int getyRacketPosition() {
        return racketPositionY;
    }

    public void setyRacketPosition(int yRacketPosition) {
        this.racketPositionY = yRacketPosition;
    }
}
